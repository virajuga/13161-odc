.data
inicio: .asciiz "\n Introdusca un numero para el calculo del factorial: "
salida: .asciiz "\n El factorial es: "

.text
main: li $v0 4
      la $a0 inicio
      syscall
      li $v0 5
      syscall
      move $t0 $v0
      beqz $t0 factorial_cero
      addi $t1 $t0 -1
      
loop: beqz $t1 factorial
      mul $t0 $t0 $t1
      addi $t1 $t1 -1
      b loop

factorial: li $v0 4
           la $a0 salida
           syscall
           move $a0 $t0
           li $v0 1
           syscall
           b exit

factorial_cero: li $t0 1
                b factorial        

exit: li $v0 10
      syscall
      
                      